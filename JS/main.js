"use strict";

let userNumber = prompt("Enter your number");
let bool = true;

while(bool){
    if(userNumber === null || userNumber === "" || isNaN(+userNumber)){
        userNumber = prompt("It's not a number");
    }else if(!Number.isInteger(+userNumber)){
        userNumber = prompt("It's not a integer number");
    }else if(userNumber < 5){
        console.log("Sorry,no numbers!");
        bool = false;
    }else{
        for(let i = 0;i <= userNumber;i++){
            if(i % 5 === 0){
                console.log(i);
            }
        }
        bool = false;
    }
}